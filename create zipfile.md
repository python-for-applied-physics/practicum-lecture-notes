---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.1'
      jupytext_version: 1.2.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
# https://stackoverflow.com/questions/458436/adding-folders-to-a-zip-file-using-python
import zipfile
from glob import glob
from datetime import datetime

outname = "Lecture_Notebooks_" + datetime.now().strftime("%Y-%m-%d") + ".zip"
outfile = zipfile.ZipFile(outname, "w")

files=[]
files += glob("*/Notebook*ipynb")
files += glob("Course_Information/*")
files += glob("Notebook*/*png")
files += glob("Notebook 5/exercise_data.dat")
files += glob("Notebook 5/v_vs_time.dat")
files += glob("Outline of notebooks.ipynb")
files += glob("Additional*/*")

for f in files:
    print(f)
    outfile.write(f,f,zipfile.ZIP_DEFLATED)

outfile.close()
```
