---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.0
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# The Python Online Classroom in TN1405-P 2020/2021

Due to the circumstances, the course in the 2020/2021 academic year will be run primarily online. Fortunately there are many great tools that will enable you to learn online, work online, and intearct with us and each other online. Particularly for programming, this can be very effective.

For the python, there are three important online tools which we will use in the following way:

* **Brightspace / Vocareum:** Where you log in and work
* **Microsoft Teams:** A "virtual classroom"
* **Support Forum:** Where you can post questions both inside and outside of lecture hours

In the sections below, I will describe in a bit more detail how we will use them.

## Brightspace / Vocareum

This is where you we will post information and the material for you to work on. Vocareum is our cloud-based computing platform, and is accessed by links from inside Brightspace. 

## Microsoft Teams

We will use Microsoft teams as an online classroom where you can work and interact with the teachers, the TAs, and with each other. 

Microsoft teams is a "chat" based communications tool that organises discussions inside a "team" into "channels", similar to the platform discord (https://discord.com/) that some of you may be familiar with from online gaming communities.

For Python, the following channels are relevant:

* **Python:** Where I will post things like instructional videos and other information that is hard to post on brightsapce
* **Tafel 01-10:** Virtual "tables" where you can work with other students using text chat, video chat, posting images / screenshots, and sharing documents
* **TA gevraagd:** A channel where you can request "live" help from a TA during lecture hours by mentioning your table name

I will post a video in the Python Teams channel illustrating this in more detail.

The video support is quite handy, it allows screen sharing, and also allows you to let others control your screen for technical support emergencies.

You are welcome to "sit together" virtually on Teams whenever you want, also outside of lecture hours! However, the course team  (TAs / teachers) will only provide support in Teams during official lecture hours.

## Support Forum

In addition, for python, we will also work with an external support forum:

https://tn1405-forum.quantumtinkerer.tudelft.nl/

You have all been sent an invitation to join the forum at your TU Delft student email address. Note you will have to create a separate password. (If you forget the password, it is easy to reset using your email address.)

The forum is a place where you can ask questions about the course material. **What is the difference between the forum and asking for help in teams?**  There are three things:

* The answers the course team provide will be visible to everyone so that people who have a similar problem can already find the answer
* You might be able to get help from other students as well who had or are having similar problems
* **Questions in the forum will also be answered outside of office hours**

Some advice and ground rules: 

* Don't be shy: just post. We will do our best to answer all questions politely and respectfully.
* Try to include as much info as you can in your post: details will help us figure out your problem. Posting error messages is highly valuable. 
* **Don't post solution code to the assignments!** Question about problems with the assignment are welcome, but please formulate your questions in words. If we get stuck trying to figure it out, we will ask you to send the code to the course team in a personal message. 
* When you post or reply, do your best to be polite and respectful: let's make this a comfortable place for everyone! 







