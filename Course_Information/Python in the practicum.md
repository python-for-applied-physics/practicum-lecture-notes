---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.0
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

<!-- #region -->
# Python in the practicum

## Overview

As part of the "Inleidend Practicum" course, you will have 5 lectures in which we will teach you the basics of programming with python.

For doing this, we have created a set of 5 "Lecture Notebooks", with the topics:

* **Notebook 1: Python Basics**
* **Notebook 2: Functions**
* **Notebook 3: Program Flow Control**
* **Notebook 4: Scientific Computing with Numpy**
* **Notebook 5: Data in Python**

There are also to additional notebooks: one with advice on good coding practices, and one on "Advanced programming concepts" for those that are interested:

* **Good coding practices** 
* **Advanced programming concepts** 

**Good coding practices** is a short notebook that you should read and apply in your assignments. **Advanced programming concepts** is optional in this course. 

For the material of Notebooks 1-3, we have an assignment connected to each notebook,  and for Notebooks 4 and 5, there will be a final project applying what you have learned to data analysis. The python component of this course makes up 15% of your final grade. Each of the assignments will count for 3 points out of 15 towards your python grade component, and the final project will count for 6 points of 15. 

* **Assignment 1 (3 points)** Due date: Tuesday, 8 Sep at 17.59
* **Assignment 2 (3 points)** Due date: Tuesday, 15 Sep at 17.59
* **Assignment 3 (3 points)** Due date: Tuesday, 22 Sep at 17.59
* **Final Project (6 points)** Due date: Friday, 25 Sep at 17.59

The assignments must be submitted before the assigned deadlines by using the "Submit" button in Vocareum.

## Platform

You will be working in an online cloud-based computing platform called "Vocareum". You will access Vocareum through the links that will be created in the brightspace course page. 

There will be information available in the course MS Teams "Python" channel about how to use Vocareum. 

##  Pace

While there are 5 lectures and 5 notebooks, we encourage you to work at your own pace through the material. More experienced programmers may cover the material more quickly, perhaps with more than one notebook worth of material per lecture, while those with less (or no) programming experience may want to go through the material more slowly and finish the material at home.  

You should make sure, however, that you cover the material at a sufficient pace that you do not miss the deadlines for the assignments. 


## What do I do with the "Lecture" notebooks? 

The lecture notebooks explain the concepts of the material we expect you to learn. They contain both "information" sections for you to read, as well as "code sections" that you can run to demonstrate these concepts live.

At the top of each notebook, we have created a summary of the learning objectives we aim to achieve.

What should I do? You should first take a look at the learning objectives at the top of the notebook to understand what we want to achieve, then you can read through the learning material and execute the code cells to see what happens.

The notebooks also contain exercises where you can test what you have learned. Some are easy, some are a bit trickier: if you get stuck with the exercises, you should not hesitate to ask a fellow student, a TA, or a lecturer for help.

Try it yourself! Feel free to add code cells to the notebook (using the "Insert" menu) to try things out. If you find something you don't understand, you are always welcome to ask a TA or a lecturer to have a look.

## Assignments

For lectures 1-3, there are "Assignment Notebooks" which we will ask you to complete.

The Assignment notebooks are designed to test if you have learned and understood the material from the lecture notebooks and have achieved the learning objective we have defined. Once you think you have mastered the material of the lecture notebooks and have played with writing code in the Try it yourself code cells and the exercise cells, you should move on to the Assignment Notebooks.

Your assignments will be submitted via Vocareum, and **must be submitted before the submission deadline**. In the week of October 5th, there will be short 10 minute examinations with the teaching assistants to determine your grade. In this examination, you will sit with a TA for 5 minutes, quickly demonstrate your code, and explain what the code does. The TA will then take 5 minutes on their own to assess the following criteria:

* The code achieves what is asked in the question
* The student is able to explain what the code is doing to the TA
* The code cells contains comments explaining what it is doing

A more detailed rubric for the assessment of assignments 1-3 will be made public when ready.

### Am I allowed to ask for help? 

Yes, of course! You are welcome to ask the teacher, the TAs, and other students for help with both the material in the Lecture Notebooks and in the Assignment Notebooks.  

Note that the TAs will be happy to help you "debug" your code  and point you  in the right  direction,  they will not give you  the answers to the assignment questions. 

###  Am  I allowed to copy my friends code? 

No! While we encourage you  to ask fellow students for help, you are not allowed to directly cut-and-paste  they  code (or copy their notebook  files). At  the  end  of the course, we will be performing a specialized plagiarism  scan (designed for computer code) on the  submitted assignments  and any anomalies will  be investigated.  

## Python Final Project

In addition to the assignments related  to the core programming concepts, we will also have a final assignment that will be connected to the material of Lecture Notebooks 4 and 5 in which you will apply your python skills for loading, plotting, and analysing data. More information on the format and assessment of the Python Final Project will follow soon. 
<!-- #endregion -->
